# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
import os
from time import sleep
import sys
import requests
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from mega import Mega
import re
import shutil
import pathlib
import ctypes
from locales import *
import locale
from subprocess import Popen, DETACHED_PROCESS
from patoolib import extract_archive
from reversal import Reversal
from worker import create_worker
from Crypto.Cipher import AES
from Crypto.Util import Counter
from crypto import (base64_to_a32, base64_url_decode, decrypt_attr, a32_to_str, get_chunks, str_to_a32)
import json
from tenacity import retry, wait_exponential, retry_if_exception_type
import random
def resource(relative_path):
    base_path = getattr(
        sys,
        '_MEIPASS',
        os.path.dirname(os.path.abspath(sys.argv[0])))
    return os.path.join(base_path, relative_path)


CHUNK_SIZE = 32768

wait = False
kill = False
is_extracting = False

# determine if application is a script file or frozen exe
if getattr(sys, 'frozen', False):
    REAL_PATH = os.path.dirname(sys.executable)
elif __file__:
    REAL_PATH = os.path.dirname(__file__)

TEST_PATH = "C:/Users/Diego/Downloads/Pokemon Essentials v21.1 2023-07-30"
test = False
path_to_use = TEST_PATH if test else REAL_PATH

# Get user language for messages
user_locale = locale.windows_locale[ ctypes.windll.kernel32.GetUserDefaultUILanguage() ].lower()
LANGUAGE = user_locale.split("_")[0] if '_' in user_locale else user_locale

SETTINGS_FILE = "pu_config"
TEMP_PATH = "temp"
current_step = None

class MegaDownloader():
    def __init__(self):
        self.sequence_num = random.randint(0, 0xFFFFFFFF)
        self.timeout = 160  # max secs to wait for resp from api requests
        self.schema = 'https'
        self.domain = 'mega.co.nz'
        self.sid = None
    def download_url(self, url, dest_path=None, dest_filename=None):
        path = self._parse_url(url).split('!')
        file_id = path[0]
        file_key = path[1]
        return self._download_file(
            file_handle=file_id,
            file_key=file_key,
            dest_path=dest_path,
            dest_filename=dest_filename,
            is_public=True,
        )
    
    @retry(retry=retry_if_exception_type(RuntimeError),
    wait=wait_exponential(multiplier=2, min=2, max=60))
    def _api_request(self, data):
        params = {'id': self.sequence_num}
        self.sequence_num += 1

        if self.sid:
            params.update({'sid': self.sid})

        # ensure input data is a list
        if not isinstance(data, list):
            data = [data]

        url = f'{self.schema}://g.api.{self.domain}/cs'
        response = requests.post(
            url,
            params=params,
            data=json.dumps(data),
            timeout=self.timeout,
        )
        json_resp = json.loads(response.text)
        try:
            if isinstance(json_resp, list):
                int_resp = json_resp[0] if isinstance(json_resp[0],
                                                      int) else None
            elif isinstance(json_resp, int):
                int_resp = json_resp
        except IndexError:
            int_resp = None
        if int_resp is not None:
            if int_resp == 0:
                return int_resp
            if int_resp == -3:
                msg = 'Request failed, retrying'
                print(msg)
            print(int_resp)
        return json_resp[0]
    
    def _download_file(self, file_handle, file_key, dest_path=None, dest_filename=None, is_public=False, file=None):
        global wait, kill
        if file is None:
            if is_public:
                file_key = base64_to_a32(file_key)
                file_data = self._api_request({
                    'a': 'g',
                    'g': 1,
                    'p': file_handle
                })
            else:
                file_data = self._api_request({
                    'a': 'g',
                    'g': 1,
                    'n': file_handle
                })

            k = (file_key[0] ^ file_key[4], file_key[1] ^ file_key[5],
                 file_key[2] ^ file_key[6], file_key[3] ^ file_key[7])
            iv = file_key[4:6] + (0, 0)
        else:
            file_data = self._api_request({'a': 'g', 'g': 1, 'n': file['h']})
            k = file['k']
            iv = file['iv']

        # Seems to happens sometime... When this occurs, files are
        # inaccessible also in the official also in the official web app.
        # Strangely, files can come back later.
        if 'g' not in file_data:
            print('File not accessible anymore')
        file_url = file_data['g']
        file_size = file_data['s']
        attribs = base64_url_decode(file_data['at'])
        attribs = decrypt_attr(attribs, k)

        if dest_filename is not None:
            file_name = dest_filename
        else:
            file_name = attribs['n']

        response = requests.get(file_url, stream=True)

        if dest_path is None:
            dest_path = ''
        else:
            dest_path += '/'
        filepath = os.path.join(dest_path, file_name)
        with open(filepath, "wb") as f:
            k_str = a32_to_str(k)
            counter = Counter.new(128, initial_value=((iv[0] << 32) + iv[1]) << 64)
            aes = AES.new(k_str, AES.MODE_CTR, counter=counter)

            for chunk in response.iter_content(CHUNK_SIZE): #chunk_start, chunk_size in get_chunks(file_size):
                #chunk = input_file.read(chunk_size)
                chunk = aes.decrypt(chunk)
                while wait:
                    if kill:
                        return
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)
                    current_size=os.path.getsize(filepath) 
                    percentage=round((int(current_size)/int(file_size))*100)
                    app.progress_label.config(text=str(percentage) + "%")
                    app.progressbar['value'] = percentage

    def _parse_url(self, url):
        """Parse file id and key from url."""
        if '/file/' in url:
            # V2 URL structure
            url = url.replace(' ', '')
            file_id = re.findall(r'\W\w\w\w\w\w\w\w\w\W', url)[0][1:-1]
            id_index = re.search(file_id, url).end()
            key = url[id_index + 1:]
            return f'{file_id}!{key}'
        elif '!' in url:
            # V1 URL structure
            match = re.findall(r'/#!(.*)', url)
            path = match[0]
            return path
        else:
            print('Url key missing')



def stream_to_file(filename, response):
    content_length = response.headers.get("content-length")
    with open(filename, "wb") as f:
        for chunk in response.iter_content(CHUNK_SIZE):
            while wait:
                if kill:
                    return
            if chunk:  # filter out keep-alive new chunks
                f.write(chunk)
                current_size=os.path.getsize(filename) 
                percentage=round((int(current_size)/int(content_length))*100)
                app.progress_label.config(text=str(percentage) + "%")
                app.progressbar['value'] = percentage

def download_from_mega(url):
    response = requests.get(url, stream=True)
    # stream_to_file(os.path.join(path_to_use, TEMP_PATH, 'game.zip'), response)
    # Seems to happens sometime... When this occurs, files are
    # inaccessible also in the official also in the official web app.
    # Strangely, files can come back later.
    if 'g' not in file_data:
        raise RequestError('File not accessible anymore')
    file_url = file_data['g']
    file_size = file_data['s']
    attribs = base64_url_decode(file_data['at'])
    attribs = decrypt_attr(attribs, k)

    file_name = attribs['n']

    input_file = requests.get(file_url, stream=True).raw

    if dest_path is None:
        dest_path = ''
    else:
        dest_path += '/'

    if kill:
        return


class GDrive():
    def download_file_from_google_drive(self, url, destination):
        URL = "https://docs.google.com/uc?export=download&confirm=1"
        session = requests.Session()
        id = self.get_id_from_url(url)
        response = session.get(URL, params={"id": id}, stream=True)
        token = self.get_confirm_token(response)
        if token:
            params = {"id": id, "confirm": token}
            response = session.get(URL, params=params, stream=True)
        if response.status_code == 200:
            self.save_response_content(response, destination)
        elif response.status_code == 404 :
            raise Exception(ExceptionMessage.FILE_NOT_ACCESSIBLE[LANGUAGE])
        else:
            raise Exception(ExceptionMessage.DOWNLOAD_ERROR[LANGUAGE])

    def get_confirm_token(self, response):
        for key, value in response.cookies.items():
            if key.startswith("download_warning"):
                return value
        return None

    def get_id_from_url(self, url):
        id = url.split("/")[5]
        return id

    def save_response_content(self, response, destination):
        content_disposition = response.headers.get("content-disposition")
        filename = re.findall("filename=(.+)", content_disposition)[0].split(';')[0].replace('"', '')
        # content_length = response.headers.get("content-length")
        filename = os.path.join(destination, filename)
        stream_to_file(filename, response)
        if kill:
            return
        app.progress_label.config(text="100%")
        app.progressbar['value'] = 100




def get_file_host(url):
    if "mega.nz" in url:
        return Host.MEGA
    elif "drive.google.com" in url:
        return Host.GOOGLE_DRIVE
    elif "mediafire.com" in url:
        return Host.MEDIAFIRE
    elif "anonfiles.com" in url:
        return Host.ANONFILES
    elif "dropbox.com" in url:
        return Host.DROPBOX
    else:
        return None
    
def donwload_from_mediafire(url):
    filename = url.split("/")[-1].replace('+', ' ')
    content = requests.get(url, stream=True)
    filename = os.path.join(path_to_use, TEMP_PATH, filename)
    stream_to_file(filename, content)
    app.progress_label.config(text="100%")
    app.progressbar['value'] = 100

def remove_updater(poke_updater_from_zip):
    command = f'ping localhost -n 5 & del /q "{os.path.realpath(sys.executable)}" ' \
            f'& move "{poke_updater_from_zip}" "{path_to_use}" ' \
            f'& rmdir /s /q "{os.path.join(path_to_use, TEMP_PATH)}"'
    Popen(command, shell=True)

def main():
    global current_step
    try: 
        # Retrieve game version and download link from settings file
        current_step = Step.RETRIEVING
        app.step_label.config(text=Step.RETRIEVING[1][LANGUAGE])
        settings_path = os.path.join(path_to_use, SETTINGS_FILE)
        with open(settings_path, encoding='utf-8') as file:
            downloaded_version = None
            pastebin_url = None
            while line := file.readline():
                if "CURRENT_GAME_VERSION" in line:
                    downloaded_version = line.split("=")[1].strip()
                if "VERSION_PASTEBIN" in line:
                    pastebin_url = line.split("=")[1].strip().replace('"', '')
                if downloaded_version and pastebin_url:
                    break 

        if not pastebin_url:
            app.show_error(ExceptionMessage.NO_PASTEBIN_URL[LANGUAGE], ExceptionMessage.CLOSE_WINDOW[LANGUAGE])
            return
        try:
            response = requests.get(pastebin_url, timeout=5)
            if response.status_code != 200:
                app.show_error(ExceptionMessage.NO_PASTEBIN_CONTENT[LANGUAGE], ExceptionMessage.CLOSE_WINDOW[LANGUAGE])
                return
            newVersion = float(response.text.split("\n")[0].strip().split("=")[1].strip())
            if not downloaded_version or newVersion <= float(downloaded_version):
                app.show_error(ExceptionMessage.NO_NEW_VERSION[LANGUAGE], ExceptionMessage.CLOSE_WINDOW[LANGUAGE])
                return
        except requests.ConnectionError:
            app.show_error(ExceptionMessage.NO_INTERNET[LANGUAGE], ExceptionMessage.CLOSE_WINDOW[LANGUAGE])
            return

        # Download new version
        current_step = Step.DOWNLOADING
        app.step_label.config(text=Step.DOWNLOADING[1][LANGUAGE])
        app.progressbar['value'] = 0
        game_url = response.text.split("\n")[1].strip().split("=")[1].strip()
        file_host = get_file_host(game_url)

        if not file_host:
            app.show_error(ExceptionMessage.NO_FILE_HOST[LANGUAGE], ExceptionMessage.CLOSE_WINDOW[LANGUAGE])
            return

        if not os.path.exists(os.path.join(path_to_use, TEMP_PATH)):
            os.mkdir(os.path.join(path_to_use, TEMP_PATH))

        try:
            if file_host == Host.MEGA:
                app.progress_label.config(text=ProgressLabel.UNKNOWN_TIME[LANGUAGE])
                mega = Mega()
                mega.login()
                MD = MegaDownloader()
                try:
                    MD.download_url(game_url, os.path.join(path_to_use, TEMP_PATH))
                except ConnectionResetError:
                    app.show_error(ExceptionMessage.DOWNLOAD_ERROR_MEGA[LANGUAGE], ExceptionMessage.CLOSE_WINDOW[LANGUAGE])
                    return
            elif file_host == Host.GOOGLE_DRIVE:
                gdrive = GDrive()
                gdrive.download_file_from_google_drive(game_url, os.path.join(path_to_use, TEMP_PATH))
            elif file_host == Host.MEDIAFIRE:
                download_url = BeautifulSoup(requests.get(game_url).content, 'html.parser').find(id="downloadButton")["href"]
                donwload_from_mediafire(download_url)
            elif file_host == Host.DROPBOX:
                pass
            if kill:
                return
        except ConnectionResetError:
            app.show_error(ExceptionMessage.DOWNLOAD_ERROR[LANGUAGE], ExceptionMessage.CLOSE_WINDOW[LANGUAGE])
            return
        except Exception as e:
            print(e)
            app.show_error(ExceptionMessage.DOWNLOAD_ERROR[LANGUAGE], ExceptionMessage.CLOSE_WINDOW[LANGUAGE])
            return
        app.progress_label.config(text="")

        # Extract files
        current_step = Step.EXTRACTING
        app.step_label.config(text=Step.EXTRACTING[1][LANGUAGE])
        app.progressbar.config(mode="indeterminate")
        app.progressbar['value'] = 0
        app.progress_label.config(text=ProgressLabel.A_FEW_SECONDS[LANGUAGE])
        app.progressbar.start()
        found_file = False
        global is_extracting
        for file in os.listdir(os.path.join(path_to_use, TEMP_PATH)):
            file_suffix = pathlib.Path(file).suffix
            if file_suffix in [".zip", ".rar", ".7z", ".tar.gz"]:
                found_file = True
                file_to_extract = os.path.join(path_to_use, TEMP_PATH, file)
                is_extracting = True
                outdir = os.path.join(path_to_use, TEMP_PATH)
                extract_archive(file_to_extract, outdir=outdir)
                break
        
        is_extracting = False
        while wait:
            if kill:
                # Reversal.reverse(current_step, os.path.join(path_to_use, TEMP_PATH))
                return

        if not found_file:
            app.show_error(ExceptionMessage.NO_VALID_FILE_FOUND[LANGUAGE], ExceptionMessage.CLOSE_WINDOW[LANGUAGE])
            return

        os.remove(file_to_extract)
        app.progress_label.config(text="")
        app.progressbar.stop()

        # Delete old files
        current_step = Step.DELETING
        app.step_label.config(text=Step.DELETING[1][LANGUAGE])
        app.progress_label.config(text=ProgressLabel.A_FEW_SECONDS[LANGUAGE])
        items_to_remove = os.listdir(path_to_use)
        total_files = sum([len(files) for _, _, files in os.walk(path_to_use)])
        deleted_items = 0
        for file in items_to_remove:
            while wait:
                if kill:
                    return
            if file.startswith(".") or file == TEMP_PATH: continue
            app.progressbar['value'] = round((deleted_items/total_files)*100)
            app.progress_label.config(text=str(app.progressbar['value']) + "%")
            if not "poke_updater" in file:
                file_to_remove = os.path.join(path_to_use,file)
                if os.path.isdir(file_to_remove):
                    deleted_tree_count = sum([len(files) for _, _, files in os.walk(os.path.join(path_to_use, file_to_remove))])
                    shutil.rmtree(file_to_remove)
                    deleted_items += deleted_tree_count
                else:
                    os.remove(file_to_remove)
                    deleted_items += 1
        app.progress_label.config(text="")

        # Move files
        current_step = Step.MOVING
        app.step_label.config(text=Step.MOVING[1][LANGUAGE])
        app.progressbar.config(mode="determinate")
        app.progressbar['value'] = 0
        app.progress_label.config(text=ProgressLabel.UNKNOWN_TIME[LANGUAGE])

        extracted_path = os.path.join(path_to_use, TEMP_PATH)
        for file in os.listdir(extracted_path):
            while wait:
                if kill:
                    return
            if os.path.isdir(os.path.join(extracted_path, file)) and not file.startswith("."):
                extracted_folder = os.path.join(extracted_path, file)
                break
        

        if not extracted_folder:
            app.show_error(ExceptionMessage.NO_VALID_FOLDER_FOUND[LANGUAGE], ExceptionMessage.CLOSE_WINDOW[LANGUAGE])
            return


        total_files = sum([len(files) for _, _, files in os.walk(extracted_folder)])
        moved_files = 0
        for file in os.listdir(extracted_folder):
            while wait:
                if kill:
                    return
            app.progressbar['value'] = round((moved_files/total_files)*100)
            app.progress_label.config(text=str(app.progressbar['value']) + "%")
            if "poke_updater" in file and file.endswith(".exe"):
                poke_updater_from_zip = os.path.join(extracted_folder, file)
            else:
                if os.path.isdir(os.path.join(extracted_folder, file)):
                    file_tree_count = sum([len(files) for _, _, files in os.walk(os.path.join(path_to_use, extracted_folder, file))])
                    moved_files += file_tree_count
                else:
                    moved_files += 1
                shutil.move(os.path.join(extracted_folder, file), path_to_use)
        app.progress_label.config(text="")

        # Post update
        for i in range(5):
            app.step_label.config(text=f'{ProgressLabel.DONE[LANGUAGE]} {str(5-i)} {ProgressLabel.SECONDS[LANGUAGE]}')
            app.update_idletasks()
            sleep(1)
        
        Popen(os.path.join(path_to_use,"Game.exe"), creationflags=DETACHED_PROCESS)
        if getattr(sys, 'frozen', False):
            remove_updater(poke_updater_from_zip)

        app.main_thread.stop()
        app.quit()
    except Exception as e: 
        print(e)
        app.show_error(ExceptionMessage.UNEXPECTED_ERROR[LANGUAGE], e)
        return

class App(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Pokemon Essentials Game Updater")
        self.resizable(False, False)
        self.progressbar = None
        self.columnconfigure(0, weight=1)
        self.iconbitmap(resource("poke_updater_logo.ico"))
        self.protocol("WM_DELETE_WINDOW", self.on_closing)
        self.create_widgets()
    
    def create_widgets(self):
        self.label = ttk.Label(self, text=ExceptionMessage.DO_NOT_CLOSE[LANGUAGE])
        self.label.grid(row=0, column=0, pady=5, padx=15, sticky='w')

        self.step_label = ttk.Label(self, text="")
        self.step_label.grid(row=1, column=0, pady=5, padx=15, sticky='w')

        self.progress_label = ttk.Label(self, text="")
        self.progress_label.grid(row=1, column=0, pady=5, padx=15, sticky='e')

        self.progressbar = ttk.Progressbar(self, orient="horizontal", mode="determinate")
        self.progressbar.grid(row=2, column=0, pady=5, padx=15, sticky=tk.E+tk.W)
    
    def show_error(self, step_text, label_text):
        self.step_label.config(text=step_text, foreground='#f00')
        self.label.config(text=label_text, foreground='#f00')
    
    def on_closing(self):
        global wait, kill, is_extracting
        wait = True
        self.main_thread.pause()
        if messagebox.askokcancel(QuitBoxTitle.TITLE[LANGUAGE], Reversal.getMessageText(current_step, LANGUAGE)):
            if is_extracting:
                messagebox.showinfo(Reversal.REVERSAL_TEXT[3][LANGUAGE][0], Reversal.REVERSAL_TEXT[3][LANGUAGE][1])
            while is_extracting:
                pass
            kill = True
            self.main_thread.stop()
            app.destroy()
            Reversal.reverse(current_step, os.path.join(path_to_use, TEMP_PATH))
        else:
            wait = False
            self.main_thread.resume()


if __name__ == "__main__":
    app = App()
    thread = create_worker(main, daemon=True) 
    app.main_thread = thread
    app.mainloop()
